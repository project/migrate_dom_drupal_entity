<?php

namespace Drupal\migrate_dom_drupal_entity\Plugin\migrate\process;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Annotation\MigrateProcessPlugin;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigratePluginManagerInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_plus\Plugin\migrate\process\DomProcessBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This plugin looks through the DOM to match a regular expression and replaces
 * the found matches with an embedded Drupal entity element.
 *
 * Available configuration keys:
 *
 * - expression: (required) This is the expression that's passed to the DOM
 *     xpath. Whatever text is found using this expression is what the entity
 *     regex group pattern will match on.
 * - entity_type: (required) The Drupal entity type.
 * - entity_regex_group: (required)
 *   - pattern: The regular expression pattern with capture groups.
 *   - groups: An array of arbitrary names associated with the captured group
 *       index.
 * - entity_id: (required)
 *   - group: The arbitrary group name based on what was defined in
 *       the entity_regex_group groups directive.
 *   - process: The entity ID migration process. Define migration plugins to
 *       determine the entity ID. Most commonly this is a migration lookup.
 * - entity_attributes: (optional)
 *   - group: The arbitrary group name based on what was defined in
 *       the entity_regex_group groups directive.
 *   - attributes: An array of arbitrary attribute keys that required
 *       transformation. Which have the following configuration keys:
 *       - name: (required) The name to use for the transformed attribute.
 *       - process: Define migration process plugins to transform the attribute
 *           value into your desired format.
 *
 * @codingStandardsIgnoreStart
 *
 * Example usage with common configuration:
 * @code
 *   - plugin: dom
 *     method: import
 *     log_messages: FALSE
 *
 *   - plugin: migrate_dom_drupal_entity
 *     expression: '//text()'
 *     entity_type: media
 *     entity_regex_group:
 *       pattern: '/\[\[nid:(\d+)(.+)?\]\]/'
 *       groups:
 *         entity_id: 1
 *         attributes: 2
 *     entity_id:
 *       group: 'entity_id'
 *       process:
 *         - plugin: migration_lookup
 *           migration:
 *             - upgrade_d7_media_image
 *     entity_attributes:
 *       group: 'attributes'
 *       attributes:
 *         field_align:
 *           name: align
 *           process:
 *             - plugin: static_map
 *               map:
 *                 _none: center
 *
 *   - plugin: dom
 *     method: export
 * @endcode
 *
 * @codingStandardsIgnoreEnd
 *
 * @MigrateProcessPlugin(
 *   id = "migrate_dom_drupal_entity"
 * )
 */
class MigrateDomDrupalEntity extends DomProcessBase implements ContainerFactoryPluginInterface {

  /** @var \Drupal\migrate\Row */
  protected $row;

  /**
   * @var \Drupal\migrate\Plugin\MigrationInterface
   */
  protected $migration;

  /**
   * @var \Drupal\migrate\MigrateExecutableInterface
   */
  protected $migrateExecutable;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var string
   */
  protected $destinationProperty;

  /**
   * @var \Drupal\migrate\Plugin\MigratePluginManagerInterface
   */
  protected $migrateProcessManager;

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration,
    EntityTypeManagerInterface $entity_type_manager,
    MigratePluginManagerInterface $migrate_process_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->migration = $migration;
    $this->entityTypeManager = $entity_type_manager;
    $this->migrateProcessManager = $migrate_process_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration = NULL
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.migrate.process')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function transform(
    $value,
    MigrateExecutableInterface $migrate_executable,
    Row $row,
    $destination_property
  ) {
    $this->init($value, $destination_property);

    $this->row = $row;
    $this->migrateExecutable = $migrate_executable;
    $this->destinationProperty = $destination_property;

    foreach ($this->requiredConfigurations() as $property) {
      if (!isset($this->configuration[$property])
        || empty($this->configuration[$property])) {

        throw new MigrateException(
          sprintf('The %s property is required!', $property)
        );
      }
    }
    $expression = $this->configuration['expression'];

    return $this->replaceExpressionWithDrupalEntity($expression);
  }

  /**
   * Define the required configurations.
   *
   * @return array
   *   An array of required configuration directives.
   */
  protected function requiredConfigurations(): array {
    return [
      'entity_id',
      'expression',
      'entity_type',
      'entity_regex_group'
    ];
  }

  /**
   * Replace expression with the Drupal entity.
   *
   * @param string $expression
   *
   * @return \DOMDocument
   *   The DOM document object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\migrate\MigrateException
   */
  protected function replaceExpressionWithDrupalEntity(
    string $expression
  ): \DOMDocument {
    /** @var \DOMElement $element */
    foreach ($this->xpath->query($expression) as $element) {
      $groups = $this->extractEntityRegexGroups($element);

      if (!isset($groups) || empty($groups)) {
        continue;
      }
      $entity_id = $this->processEntityId($groups);

      if (!isset($entity_id)) {
        continue;
      }
      $entity_type = $this->getEntityType();
      $entity_attributes = $this->processEntityAttributes($groups);

      $drupal_entity = $this->createDrupalEntityElement(
        $entity_id, $entity_type, $entity_attributes
      );

      if (!$drupal_entity) {
        continue;
      }

      $element->parentNode->replaceChild($drupal_entity, $element);
    }

    return $this->document;
  }

  /**
   * Process the entity ID.
   *
   * @param array $groups
   *   An array of the group contexts.
   *
   * @return null|string
   *   The processed entity identifier.
   *
   * @throws \Drupal\migrate\MigrateException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function processEntityId(array $groups): ?string {
    if (!isset($this->configuration['entity_id'])) {
      throw new MigrateException(
        'The entity_id definition is required.'
      );
    }
    $definition = $this->configuration['entity_id'];

    return $this->processDefinitionGroupValue($definition, $groups);
  }

  /**
   * Process the entity attributes.
   *
   * @param array $groups
   *   An array of the group contexts.
   *
   * @return array
   *   The processed entity attributes.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\migrate\MigrateException
   */
  protected function processEntityAttributes(array $groups): array {
    $definition = $this->configuration['entity_attributes'] ?? [];
    return $this->processDefinitionAttributes($definition, $groups);
  }

  /**
   * Create the Drupal entity element.
   *
   * @param $entity_id
   *   The entity identifier.
   * @param $entity_type
   *   The entity type.
   * @param array $attributes
   *   An array of attributes to attach.
   *
   * @return bool|\DOMElement
   *   The Drupal entity element.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createDrupalEntityElement(
    string $entity_id,
    string $entity_type,
    array $attributes = []
  ) {
    if (!is_numeric($entity_id)) {
      return FALSE;
    }
    var_dump($entity_id);
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->entityTypeManager
      ->getStorage($entity_type)
      ->load($entity_id);

    var_dump($entity);

    if (!$entity instanceof EntityInterface) {
      return FALSE;
    }
    $element = $this->document->createElement('drupal-entity');

    $attributes = [
        'data-entity-uuid' => $entity->uuid(),
        'data-entity-type' => $entity->getEntityTypeId(),
      ] + $attributes;

    foreach ($attributes as $key => $value) {
      if (strpos($key, 'data-') !== 0) {
        $key = "data-{$key}";
      }
      $element->setAttribute($key, $value);
    }

    return $element;
  }

  /**
   * Extract the entity regex groups from the DOM node.
   *
   * @param \DOMNode $node
   *   The DOM node object.
   *
   * @return array
   *
   * @throws \Drupal\migrate\MigrateException
   */
  protected function extractEntityRegexGroups(\DOMNode $node): array {
    $entity_regex_group = $this->configuration['entity_regex_group'];

    if (!isset($entity_regex_group['pattern'])) {
      throw new MigrateException(
        'The entity regex group pattern directive is required!'
      );
    }
    $matches = [];
    $content = trim($node->textContent);

    if (preg_match($entity_regex_group['pattern'], $content, $matches)) {
      $groups = [];

      foreach ($entity_regex_group['groups'] as $name => $index) {
        if (!isset($matches[$index])) {
          continue;
        }
        $groups[$name] = $matches[$index];
      }

      return $groups;
    }

    return [];
  }

  /**
   * Process the definition attributes.
   *
   * @param array $definition
   *   A definition array.
   * @param array $groups
   *   An array of the group contexts.
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\migrate\MigrateException
   */
  protected function processDefinitionAttributes(
    array $definition,
    array $groups
  ): array {
    if (
      !isset($definition['group'], $definition['attributes'])
      || empty($definition['attributes'])
    ) {
      return [];
    }
    $group = $definition['group'] ?? NULL;

    if (!isset($groups[$group])) {
      throw new MigrateException('Definition group is invalid!');
    }
    $attributes = [];
    $captured_attributes = $this->formatAttributes($groups[$group]);

    foreach ($definition['attributes'] as $key => $attribute) {
      if (!isset($attribute['name'], $attribute['process'], $captured_attributes[$key])) {
        continue;
      }
      $name = $attribute['name'];
      $value = $captured_attributes[$key];

      $attributes[$name] = $this->processDefinitionValue(
        $attribute,
        $value
      );
    }

    return $attributes;
  }

  /**
   * Process definition group value.
   *
   * @param array $definition
   *   A definition array.
   * @param array $groups
   *   An array of the group contexts.
   *
   * @return null|string
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\migrate\MigrateException
   */
  protected function processDefinitionGroupValue(
    array $definition,
    array $groups
  ): ?string {
    if (($group = $definition['group']) && !isset($groups[$group])) {
      throw new MigrateException('Definition group is invalid!');
    }
    $value = $groups[$group];

    return $this->processDefinitionValue($definition, $value);
  }

  /**
   * Process the definition value.
   *
   * @param array $definition
   *   A definition array containing the process.
   * @param $value
   *   The value to process.
   *
   * @return null|string
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function processDefinitionValue(
    array $definition,
    string $value
  ): ?string {
    if (!isset($definition['process'])) {
      return $value;
    }
    $process = $definition['process'];

    if (!is_array($process)) {
      $process = [$process];
    }

    return $this->invokeMigrateProcessTransform($value, $process);
  }

  /**
   * Invoke the migrate process transformation.
   *
   * @param $value
   *   The value to transform.
   * @param array $processes
   *   An array of migrate process plugins.
   *
   * @return string
   *   The value that's been transformed by all process plugins.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function invokeMigrateProcessTransform(
    string $value,
    array $processes
  ): ?string {
    foreach ($processes as $process) {
      if (!isset($process['plugin'])) {
        continue;
      }
      $plugin_id = $process['plugin'];

      unset($process['plugin']);
      $configurations = $process;

      /** @var \Drupal\migrate\ProcessPluginBase $instance */
      $instance = $this->migrateProcessManager->createInstance(
        $plugin_id,
        $configurations,
        $this->migration
      );

      $value = $instance->transform(
        $value,
        $this->migrateExecutable,
        $this->row,
        $this->destinationProperty
      );
    }

    return $value;
  }

  /**
   * Format HTML attributes from a string.
   *
   * @param string $string
   *   A string of HTML attributes.
   * @param string $delimiter
   *   A delimiter that's between each attribute.
   *
   * @return array
   *   A single dimension array of key/value pairs.
   */
  protected function formatAttributes(
    string $string,
    string $delimiter = '&'
  ): array {
    $attributes = [];

    foreach (explode($delimiter, trim($string)) as $attribute) {
      [$key, $value] = array_map('trim', explode('=', $attribute));

      $attributes[$key] = $value;
    }

    return $attributes;
  }

  /**
   * Get the Drupal entity type.
   *
   * @return string
   *   The entity type.
   */
  protected function getEntityType(): ?string {
    return $this->configuration['entity_type'] ?? NULL;
  }
}
